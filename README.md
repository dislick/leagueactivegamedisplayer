# README #

### What is this repository for? ###

* Automatically starting LoLNexus (or any other website) on your TV whenever a player starts a game
* Your TV does **not** need to be switched on, it uses `cec-client` from libCEC to turn it on

### What do I need? ###

* Raspberry Pi with Raspbian Wheezy installed
* [libCEC installed on the RPi](http://www.raspberrypi.org/forums/viewtopic.php?f=29&t=70923) (make sure the `cec-client` command is available)
* [Node.js installed on the RPi](http://revryl.com/2014/01/04/nodejs-raspberry-pi/)

### How do I start the program? ###

* `git clone https://dislick@bitbucket.org/dislick/leagueactivegamedisplayer.git`
* `npm install`
* `npm install -g forever`
* `forever start app.js` to start a deamon which runs longer than your SSH session