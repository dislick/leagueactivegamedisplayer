var request = require('request');
var exec = require('child_process').exec;

var config = (function() {
  var summonerName   = 'hi im muff'.replace(/\s*/g, ''),
      server         = 'EUW',
      updateInterval = 1000,
      websiteUrl     = 'http://www.lolnexus.com/' + server + '/search?name=' + summonerName + '&region=EUW';
    
  return { 
    summonerName:       summonerName,
    server:             server,
    updateInterval:     updateInterval,
    websiteUrl:         websiteUrl,
    currentIngameState: false,
    lastGameStart:      new Date(),
    minGameDuration:    15 * 60 * 1000,
  }
})();

var isSummonerIngame = function(callback) {
  request('http://www.lolnexus.com/ajax/get-game-info/' + config.server + '.json?name=' + config.summonerName, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      try {
        var parsedData = JSON.parse(body);
        callback(parsedData.successful, parsedData.html);
      } catch (exception) {
        console.log(exception);
        callback(false);
      }
    } else {
      callback(false);
    }
  });
};

var parseLolnexusHtml = function(html) {
  var getQueueType = /<h2>[\w\s]+<small>(.+)<\/small><\/h2>/;
  var queueType = html.match(getQueueType)[1];

  console.log(config.summonerName + ' just started the following game: ' + queueType);
};

var executeCommand = function(command) {
  exec(command, function() { /* no interest in callback */ }); 
};

var startWebsite = function() {
  config.lastGameStart = new Date();

  console.log('Starting TV');
  executeCommand('echo "on 0" | cec-client -s');
  executeCommand('startx');
  executeCommand('midori -e Fullscreen --display :0 -a ' + config.websiteUrl);
};

var stopWebsite = function() {
  // check if game has a min duration. this is to prevent on/off switching because of lolnexus bugs
  if (new Date().getTime() - config.lastGameStart.getTime() > config.minGameDuration) {
    return;
  }

  console.log('Stopping TV');
  executeCommand('echo "standby 0" | cec-client -s');
  executeCommand('killall -9 midori');
  executeCommand('killall -9 lxsession'); 
};

(function next() {
  setTimeout(function() {
    isSummonerIngame(function(isIngame, html) {
      console.log('Summoner ingame check: ' + isIngame);

      if (isIngame && !config.currentIngameState) {
        startWebsite();
        parseLolnexusHtml(html);
      }
       
      config.currentIngameState = isIngame;

      if (!isIngame && config.currentIngameState) {
        stopWebsite();
      }

      next();
    });
  }, config.updateInterval);
})();
